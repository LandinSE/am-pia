import { RestaurantesService } from './../restaurantes/restaurantes.service';
import { Restaurante } from './restaurante.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.page.html',
  styleUrls: ['./detalles.page.scss'],
})
export class DetallesPage implements OnInit {

  restauranteActual: Restaurante;
  public calificacionProm: number;
  nombre: string;
  menu: string[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private RestaurantesService: RestaurantesService,
    private router: Router,
    private alertCtrl: AlertController
    
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      const param: string = 'restauranteId';
      if(!paramMap.has(param)){
        this.router.navigate(['/detalles'])
        return;
      }
      const restauranteId: number = +paramMap.get(param);
      this.restauranteActual = this.RestaurantesService.getRestaurante(restauranteId);
      this.calificacionProm = this.restauranteActual.calificacion.reduce((a,b) => a + b, 0)/this.restauranteActual.calificacion.length;
      this.nombre = this.restauranteActual.nombre;
      this.menu = this.restauranteActual.menu;
    });
  }

  Guardar(){

  }
}
