export class Restaurante{ 
    constructor(
        public id: number,
        public nombre: string,
        public direccion: string,
        public tipoComida: string,
        public imagen: string,
        public calificacionUsuario: number,
        public calificacion: number[],
        public menu: string[]
    ){}
}