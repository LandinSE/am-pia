import { Restaurante } from './../detalles/restaurante.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RestaurantesService {
  restaurantes: Restaurante[] = [
    {
      id: 1,
      nombre: 'Hamburguesas El Meme',
      direccion: 'Calle Alejandro de Rodas 5708, Cumbres Las Palmas Residencial, 64349 Monterrey, N.L.',
      tipoComida: 'Hamburgesas',
      imagen: 'https://sifu.unileversolutions.com/image/es-MX/recipe-topvisual/2/1260-709/hamburguesa-clasica-50425188.jpg',
      calificacionUsuario: 0,
      calificacion: [5,5,5,4,3],
      menu: ['Burger']
    },
    {
      id: 2,
      nombre: 'Carnes Toño',
      direccion: 'Hda. La Estrella 5737, Colonial Cumbres, 64349 Monterrey, N.L.',
      tipoComida: 'Carnes y Tacos',
      imagen: 'https://www.simplyrecipes.com/wp-content/uploads/2007/05/carne-asada-horiz-a-1400.jpg',
      calificacionUsuario: 0,
      calificacion: [5,5,3,5,3],
      menu: ['Sirloin', 'Ribeye', 'New York']
    },
    {
      id: 3,
      nombre: 'Mama Luigi',
      direccion: 'Av Paseo de los Leones 3201, Cumbres 6o. Sector Secc a, 64619 Monterrey, N.L.',
      tipoComida: 'Italiana',
      imagen: 'https://gestion.pe/resizer/AkkRZEBwJlar9StwQdov7Azs2MY=/580x330/smart/filters:format(jpeg):quality(75)/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/HUX6QQCCYZCZFICTNWVTGUD4S4.jpg',
      calificacionUsuario: 0,
      calificacion: [5,3,4,3,5],
      menu: ['Spaghetti', 'Pizza']
    },
    {
      id: 4,
      nombre: 'Jochos Alex',
      direccion: 'Av Paseo de los Leones 2973, Cumbres 5o. Sector Secc B, 64619 Monterrey, N.L.',
      tipoComida: 'Americana',
      imagen: 'https://www.superama.com.mx/views/micrositio/recetas/images/backtoschool/hotdogs/Web_fotoreceta.jpg',
      calificacionUsuario: 0,
      calificacion: [5,4,3,5,3],
      menu: ['Hot Dog']
    },
    {
      id: 5,
      nombre: 'Pho-nganos Diez comida Vietnamita',
      direccion: 'Calle Paseo de Los Triunfadores 3106, Cumbres Octavo Sector, 64610 Monterrey, N.L.',
      tipoComida: 'Asiatica',
      imagen: 'https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2018/05/receta-facil-de-comida-china-casera.jpg',
      calificacionUsuario: 0,
      calificacion: [5,5,5,5,5],
      menu: ['Dumplings', 'Orange Chicken']
    }
  ];

  constructor() { }

  getAllRestaurantes(){
    return [...this.restaurantes];
  }

  getRestaurante(restauranteId: number){
    return {...this.restaurantes.find(r => {
      return r.id === restauranteId;
    })
    }
  }
}
