import { RestaurantesService } from './../restaurantes/restaurantes.service';
import { Restaurante } from './../detalles/restaurante.model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: 'inicio.page.html',
  styleUrls: ['Inicio.page.scss']
})
export class InicioPage {

  restaurantes: Restaurante[];

  constructor(private RestaurantesService: RestaurantesService) {}

  ngOnInit() {
    this.restaurantes = this.RestaurantesService.getAllRestaurantes();
  }
}
