import { Component } from '@angular/core';
import { RestaurantesService } from './../restaurantes/restaurantes.service';
import { Restaurante } from './../detalles/restaurante.model';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  restaurantes: Restaurante[];

  constructor(private RestaurantesService: RestaurantesService) {}

  ngOnInit() {
    this.restaurantes = this.RestaurantesService.getAllRestaurantes();
  }

  onRateChange(event: number) {
    console.log('Your rate:', event);
  }
}
